<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content ehc ehc-results">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'ehc-calculator' );

				endwhile;
			?>
			<div class="entry-content">
				<div class="viscosity result-section">
					<h2>Viscosity @ 40&deg; C ASTM D-445</h2>
					<div class="viscosity result secondary">
						<h3>No Input</h3>
						<p>You did not select a range for Viscosity</p>
					</div>
					<div class="viscosity result least">
						<h3>Least Risk - <span class="vis-value"></span></h3>
						<p><?php echo get_field('viscosity_least_risk');?></p>
					</div>
					<div class="viscosity result less">
						<h3>Less Risk - <span class="vis-value"></span></h3>
						<p><?php echo get_field('viscosity_less_risk');?></p>
					</div>
					<div class="viscosity result caution">
						<h3>Caution - <span class="vis-value"></span></h3>
						<p><?php echo get_field('viscosity_caution');?></p>
					</div>
					<div class="viscosity result more">
						<h3>More Risk - Needs Recovery - <span class="vis-value"></span></h3>
						<p><?php echo get_field('viscosity_more_risk');?></p>
					</div>
					<div class="viscosity result most">
						<h3>Most Risk - Must Recover - <span class="vis-value"></span></h3>
						<p><?php echo get_field('viscosity_most_risk');?></p>
					</div>
				</div>

        <div class="acid result-section">
          <h2>Total Acid/Neutralization No. (mg KOH / gm)</h2>
					<div class="acid result secondary">
						<h3>No Input</h3>
						<p>You did not select a range for Total Acid/Neutralization No.</p>
					</div>
          <div class="acid result least">
            <h3>Least Risk - <span class="acid-value"></span> mg KOH / gm</h3>
            <p><?php echo get_field('acid_number_least_risk');?></p>
          </div>
          <div class="acid result less">
            <h3>Less Risk - <span class="acid-value"></span> mg KOH / gm</h3>
            <p><?php echo get_field('acid_number_less_risk');?></p>
          </div>
          <div class="acid result caution">
            <h3>Caution - <span class="acid-value"></span> mg KOH / gm</h3>
            <p><?php echo get_field('acid_number_caution');?></p>
          </div>
          <div class="acid result more">
            <h3>More Risk - Needs Recovery - <span class="acid-value"></span> mg KOH / gm</h3>
            <p><?php echo get_field('acid_number_more_risk');?></p>
          </div>
          <div class="acid result most">
            <h3>Most Risk - Must Recover - <span class="acid-value"></span> mg KOH / gm</h3>
            <p><?php echo get_field('acid_number_most_risk');?></p>
          </div>
        </div>

        <div class="water result-section">
          <h2>Water Content ppm</h2>
					<div class="water result secondary">
						<h3>No Input</h3>
						<p>You did not select a range for Water Content</p>
					</div>
          <div class="water result least">
            <h3>Least Risk - <span class="water-value"></span> ppm</h3>
            <p><?php echo get_field('water_least_risk');?></p>
          </div>
          <div class="water result less">
            <h3>Less Risk - <span class="water-value"></span> ppm</h3>
            <p><?php echo get_field('water_less_risk');?></p>
          </div>
          <div class="water result caution">
            <h3>Caution - <span class="water-value"></span> ppm</h3>
            <p><?php echo get_field('water_caution');?></p>
          </div>
          <div class="water result more">
            <h3>More Risk - Needs Recovery - <span class="water-value"></span> ppm</h3>
            <p><?php echo get_field('water_more_risk');?></p>
          </div>
          <div class="water result most">
            <h3>Most Risk - Must Recover - <span class="water-value"></span> ppm</h3>
            <p><?php echo get_field('water_most_risk');?></p>
          </div>
        </div>

        <div class="resistivity result-section">
          <h2>Resistivity Gohm.cm</h2>
					<div class="resistivity result secondary">
						<h3>No Input</h3>
						<p>You did not select a range for Resistivity</p>
					</div>
          <div class="resistivity result least">
            <h3>Least Risk - <span class="resist-value"></span> Gohm.cm</h3>
            <p><?php echo get_field('resistivity_least_risk');?></p>
          </div>
          <div class="resistivity result less">
            <h3>Less Risk - <span class="resist-value"></span> Gohm.cm</h3>
            <p><?php echo get_field('resistivity_less_risk');?></p>
          </div>
          <div class="resistivity result caution">
            <h3>Caution - <span class="resist-value"></span> Gohm.cm</h3>
            <p><?php echo get_field('resistivity_caution');?></p>
          </div>
          <div class="resistivity result more">
            <h3>More Risk - Needs Recovery - <span class="resist-value"></span> Gohm.cm</h3>
            <p><?php echo get_field('resistivity_more_risk');?></p>
          </div>
          <div class="resistivity result most">
            <h3>Most Risk - Must Recover - <span class="resist-value"></span> Gohm.cm</h3>
            <p><?php echo get_field('resistivity_most_risk');?></p>
          </div>
        </div>

			</div>



		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
?>

<script>
	(function($) {
		var results = getUrlVars();
		console.log(results);
		var vis = parseFloat(results['vis']).toFixed(1)
		var acid = parseFloat(results['acid']).toFixed(2)
		var water = parseFloat(results['water']).toFixed(0)
		var resistivity = parseFloat(results['resist']).toFixed(1)
		console.log(vis)
    $(".resist-value").html(resistivity)
    $(".water-value").html(water)
    $(".acid-value").html(acid)
    $(".vis-value").html(vis)

		if(vis == undefined) {
			document.location.href = '/ehc-calculator'
		}

		switch(true) {
			case 'secondary' :
				$('.viscosity.secondary').css('display','block');
				break;
			case (vis >= 43.7 && vis <= 48.3) :
				$('.viscosity.least').css('display','block');
				break;
			case ((vis >= 42.6 && vis <= 43.6) || (vis >= 48.4 && vis <= 49.5)) :
				$('.viscosity.less').css('display','block');
				break;
			case ((vis >= 41.4 && vis <= 42.5) || (vis >= 49.6 && vis <= 50.6)) :
				$('.viscosity.caution').css('display','block');
				break;
			case ((vis >= 39.1 && vis <= 41.3) || (vis >= 50.7 && vis <= 52.9)) :
				$('.viscosity.more').css('display','block');
				break;
			case (vis <= 39 || vis >= 53) :
				$('.viscosity.most').css('display','block');
				break;
      default:
        console.log(vis)
		}

    switch(true) {
      case 'secondary' :
        $('.water.secondary').css('display','block');
        break;
      case (water < 200) :
        $('.water.least').css('display','block');
        break;
      case (water >=200 && water <=599) :
        $('.water.less').css('display','block');
        break;
      case (water >=600 && water <=999) :
        $('.water.caution').css('display','block');
        break;
      case (water >= 1000 && water <= 1500) :
        $('.water.more').css('display','block');
        break;
      case (water > 1500) :
        $('.water.most').css('display','block');
        break;
    }

    switch(true) {
      case 'secondary' :
        $('.acid.secondary').css('display','block');
        break;
      case (acid <= 0.1) :
        $('.acid.least').css('display','block');
        break;
      case (acid > 0.1 && acid <=0.15) :
        $('.acid.less').css('display','block');
        break;
      case (acid > 0.15 && acid <= 0.2) :
        $('.acid.caution').css('display','block');
        break;
      case (acid > 0.2 && acid <= 0.25) :
        $('.acid.more').css('display','block');
        break;
      case (acid > 0.25) :
        $('.acid.most').css('display','block');
        break;
    }



    switch(true) {
      case 'secondary' :
        $('.resistivity.secondary').css('display','block');
        break;
      case (resistivity > 20) :
        $('.resistivity.least').css('display','block');
        break;
      case (resistivity >=10 && resistivity <= 20) :
        $('.resistivity.less').css('display','block');
        break;
      case (resistivity >=5 && resistivity <= 9) :
        $('.resistivity.caution').css('display','block');
        break;
      case (resistivity >=1 && resistivity <= 4) :
        $('.resistivity.more').css('display','block');
        break;
      case (resistivity < 1) :
        $('.resistivity.most').css('display','block');
        break;
    }
	})(jQuery);

	function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++){
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
}
</script>
