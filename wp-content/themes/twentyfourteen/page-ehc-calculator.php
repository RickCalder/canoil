<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content ehc">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'ehc-calculator' );

				endwhile;
			?>

		  <div class="entry-content">
		    <form>
		      <div class="row" style="margin-top: 2em;">
		        <div class="col-sm">
		          <label for="viscosity">Viscosity cSt @ 40&deg; C <span class="info water" data-div="viscosity-definition">?</span></label>
		          <input type="text" id="viscosity" class="form-control" data-title="Viscoisty" style="text-align: right">
		          <p class="error vis-error">This field must be numeric and is required</p>
					    <div class="viscosity-definition definition">
					    	<?php echo get_field('viscosity_definition');?>
					    </div>
		        </div>
		        <div class="col-sm">
		          <label for="acid">Total Acid/Neutralization No. (mg KOH / gm) <span class="info water" data-div="acid-definition">?</span></label>
		          <input type="text" id="acid" class="form-control" data-title="Acid Number" style="text-align: right">
		          <p class="error acid-error">This field must be numeric and is required</p>
					    <div class="acid-definition definition">
					    	<?php echo get_field('acid_number_definition');?>
					    </div>
		        </div>
		        <div class="col-sm">
		          <label for="resistivity">Resistivity Gohm.cm <span class="info water" data-div="resistivity-definition">?</span></label>
		          <input type="text" id="resistivity" class="form-control" data-title="Resistivity" style="text-align: right">
		          <p class="error resist-error">This field must be numeric and is required</p>
					    <div class="resistivity-definition definition">
					    	<?php echo get_field('resistivity_definition');?>
					    </div>
		        </div>
		        <div class="col-sm">
		          <label for="water">Water Content ppm <span class="info water" data-div="water-definition">?</span></label>
		          <input type="text" id="water" class="form-control" data-title="Water Content pwt%" style="text-align: right">
		          <p class="error water-error">This field must be numeric and is required</p>
					    <div class="water-definition definition">
					    	<?php echo get_field('water_content_definition');?>
					    </div>
		        </div>
		      </div>
		      <button type="submit" id="testSubmit" class="btn btn-primary" style="margin-top: 2em;">Calculate</button>
		    </form>
		    <div id="background" class="row" style="margin-top:2em">
		      <h2>Background</h2>
		      <?php 
		      echo get_field('background');
		      ?>
	      </div>
		    <div id="disclaimer" class="row" style="margin-top:2em;">
		      <h2>Disclaimer</h2>
		      <?php 
		      echo get_field('disclaimer');
		      ?>
	      </div>
		  </div>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
?>

<script>
	(function($) {
		$('.info').on('click', function(e) {
			e.preventDefault()
			var div = $('.' + $(this).data('div'))

			div.slideToggle()
		})

		$('#testSubmit').on('click', function(e) {
			e.preventDefault();
			$(".error").slideUp()
			var errors = 0
			if(! $.isNumeric($('#viscosity').val())) {
				$(".vis-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#acid').val())) {
				$(".acid-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#resistivity').val())) {
				$(".resist-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#water').val())) {
				$(".water-error").slideDown()
				errors +=1
			}
			if(errors > 0) {
				return false
			}
			var vis = $('#viscosity').val()
			var acid = $('#acid').val()
			var resist = $('#resistivity').val()
			var water = $('#water').val()
			var result = '?vis=' + vis + '&acid=' + acid + '&water=' + water + '&resist=' + resist
			document.location.href = '/ehc-results' + result;
			console.log(result)
		})
	})(jQuery);
</script>
